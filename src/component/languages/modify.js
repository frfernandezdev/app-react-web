import React, { Component } from 'react';
import {
	MenuItem,
	TextField,
	Typography,
	Paper,
	Grid,
	Icon,
	IconButton, 
	FormControl,
	Input,
	InputLabel,
	InputAdornment
} from '@material-ui/core';
import * as FontAwesome from 'react-icons/lib/fa';

import {
  SelectLocalities,
	SelectTimeZone,
	SelectCurrentMoney,
	SelectLanguage,
	SelectLanguageNative,
	UploadButton,
	DateNow
} from '../../utils';

class Modify extends Component {
	state = {
		open: false,
		form: {
			tools: {}
		},
		send: {
			tools: {}
		},
		showpassword: false
	}
	constructor(props){
		super(props)

		this.firestore = props.firebase.firestore().collection('language');
		this.storage   = props.firebase.storage();
		this.onSubmit  = this.onSubmit.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.handleOnClick = this.handleOnClick.bind(this)
		this.props.handleOnClick(this.handleOnClick)
	}
	componentWillMount(){
		if(
			this.props.location.state !== undefined	 
				&&
			'id' in this.props.location.state
				&&
			this.props.location.state.id !== undefined
		){
			console.log('here')
			
			this.setState({ uid: this.props.location.state.id }, () => this.handleDateInfo())
		}
	}
	componentWillUpdate(prevProps ,prevState){
		if(
			prevProps.location.state !== undefined 
				&& 
			'id' in prevProps.location.state 
				&&
			prevProps.location.state.id !== undefined
				&& 
			prevState.uid !== prevProps.location.state.id
		){
			this.setState(
				{ 
					uid: prevProps.location.state.id,
					send: { tools: {} } 
				}, () => this.handleDateInfo())
		}
	}
	handleDateInfo = () => {
		this.firestore
			.doc(this.state.uid)
			.onSnapshot(snapshot => {
				if(snapshot.exists){
					this.setState({
						form: {
							...snapshot.data(),
							id: snapshot.id
						}
					})
				}else console.log('Not Found Users')
			}, error => console.log(error))
	}
	handleChange = name => event => {
		if(this.state.form[name] === this.state.send[name])
			return;
		this.setState({
			send: {
				...this.state.send,
				[name]: event.target.value
			}
		})
	}
	onSubmit = event => {
		event.preventDefault();
		this.props.handleOnLoading('show')
		console.log(this.state.send)
		fetch(
			'https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/languages/'+this.state.uid,
			{
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'PUT',
				mode: 'cors',
				body: JSON.stringify({
					token: this.props.user.auth.token,
					...this.state.send
				})
			}
		)
		.then(response => response.json())
		.then(response => {
			if(response.success === true){
				this.props.handleSnackbar({type: 'custom', message: response.mes})
				setTimeout(() => {
					this.props.handleOnLoading('hide')
					this.props.handleChange(null, 0) 
				}, 2500)
				this.props.handleOnLoading('done')
			}else {
				this.props.handleOnLoading('hide')
				if(response.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			this.props.handleOnLoading('hide')
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
	}
	handleOnClick = () => {
		document.getElementById('modify/submit').click()
    }
    render() {
		const { id, value } = this.props;
        const { showpassword } = this.state;
        return (
            <Paper style={{padding: 15}}>
                <form 
                    id="modify"
                    onSubmit={this.onSubmit}
										style={{ display: value === id ? 'block' : 'none' }}
                > 
								<Typography variant="title" gutterBottom>Editar lenguaje</Typography>
									<Grid container>
										<Grid
												item 
												xs={12}
												sm={12}
												md={12}
												lg={12}
												xl={12}
												style={{
													paddingLeft: 10,
													paddingRight: 10
												}}
										>
									</Grid>
									<Grid 
										item 
										xs={12} 
										sm={12} 
										md={6} 
										lg={6} 
										xl={6} 
										style={{
											paddingLeft: 10,
											paddingRight: 10
										}}
									>
										<TextField
											id='modify/name'
											label="Nombre"
											margin="normal"
											fullWidth
											// required
											// error={this.state.error.username}
											type='text'
											value={ 
												this.state.send.hasOwnProperty('name')
													?
												this.state.send.name
													:
												this.state.form.name 
													? 
												this.state.form.name 
													: 
												''
											}
											onChange={this.handleChange('name')}
										/>
											
									</Grid>
									<Grid 
										item 
										xs={12} 
										sm={12} 
										md={6} 
										lg={6} 
										xl={6} 
										style={{
											paddingLeft: 10,
											paddingRight: 10
										}}
									>
										<TextField
											id="modify/acronym"
											label="Acronymo"
											margin="normal"
											fullWidth
											// required
											// error={this.state.error.email}
											value={
												this.state.send.hasOwnProperty('acronym')
													?
												this.state.send.acronym
													:
												this.state.form.acronym
													? 
												this.state.form.acronym 
													: 
												''
											}
											type='text'
											onChange={this.handleChange('acronym')}
										/>
									</Grid>	
									<button type="submit" id="modify/submit" style={{display: 'none'}}>Enviar</button>
								</Grid>
							</form>
            </Paper> 
        )		
	}  
}                      
export default Modify;    