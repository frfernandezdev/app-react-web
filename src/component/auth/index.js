import React, { Component } from 'react';
import { 
	Grid,
	Paper,
	AppBar,
	Icon,
	Tabs,
	Tab
} from '@material-ui/core';
import SwipeableViews from 'react-swipeable-views';

import Login from './login';
import Register from './register';

import './index.css';

class Auth extends Component {
	state = {
		value: 0,
		open: false,
		msj: null
	}
	componentDidMount() {
		document.title = 'Iniciar Sesión - SpeakEnglishSite'
		window.addEventListener('scroll', this.handleScroll);
	}
	handleChange = (event, value) => {
		this.setState({ value })
	}
	render() {
		const height = window.innerHeight - 1;
		return (
			<div 
				className="background" 
			>
				<Grid container className="container-fluid">
					<Grid 
						item 
						xs={12} 
						sm={8} 
						md={8} 
						lg={9} 
						xl={9} 
						className="p-0"
					>
					</Grid>
					<Grid
						id="grid-form-login" 
						item 
						xs={12} 
						sm={4} 
						md={4} 
						lg={3}
						xl={3}
						className="p-0"
					>
						<Paper className="paper">
							<Grid
								className="content"
								container
								direction='column'
								justify='center'
							>
								<Grid className="swipeableviews">
									<SwipeableViews index={this.state.value}>
										<Login 
											{...this.props}  
											tabs={this.handleChange}
										/>
										<Register 
											{...this.props} 
											tabs={this.handleChange}
										/>
									</SwipeableViews> 
								</Grid>
								<Grid className="appbar-bottom">
									<AppBar position="static" color="default">
										<Tabs
											value={this.state.value}
											onChange={this.handleChange}
											indicatorColor="primary"
											textColor="primary"
											fullWidth
											centered
										>
											<Tab label="Iniciar Sesión" />
											<Tab label="Registrate" />
										</Tabs>
									</AppBar>
								</Grid>
							</Grid>
						</Paper>
					</Grid>
				</Grid>
			</div>
		);
	}
}

export default Auth;