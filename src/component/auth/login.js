import React, { Component } from 'react';
import {
	TextField,
	Typography,
	Button,
	CircularProgress
} from '@material-ui/core';

class Login extends Component {
	state = {
		form: {},
		loading: 'Iniciar Sesíon'
	}
	constructor(props){
		super()

		this.auth 			   = props.firebase.auth();
		this.onSubmit      = this.onSubmit.bind(this);
		this.handleOnClick = this.handleOnClick.bind(this);
	}
	onSubmit(e){
		e.preventDefault();
		let email = this.state.form.email,
				password = this.state.form.password;

		this.setState({loading: <CircularProgress size={20} color='inherit' />})

		this.auth
			.signInWithEmailAndPassword(email, password)
			.then(user => {
				if(user) {
					this.props.snackbarLoading();
					return this.setState({ loading: 'Iniciar Sesíon' });
				}
			})
			.catch(error => {
				console.log(error)
				this.setState({ loading: 'Iniciar Sesíon' })
				if(!error)
					return this.props.snackbar({type: 'auth',message: error.code , on: true})
				return this.props.snackbar({type: 'custom', message: '404', on: true})
			})
	}
	handleChange = name => event => {
		this.setState({
			form: {
				...this.state.form,
				[name]: event.target.value
			}
		})
	}
	handleOnClick = () => {
		document.getElementById('login').click()
	}
	render(){
		const { form } = this.state;
		return (
			<div className='form'>
				<form onSubmit={this.onSubmit}>
					<Typography variant="title" gutterBottom>Iniciar Sesión</Typography>
					<TextField
						id="login/mail"
						label="Correo Electronico"
						margin="normal"
						fullWidth={true}
						required={true}
						type='email'
						value={form.email ? form.email : ''}
						onChange={this.handleChange('email')}
					/>
					<TextField
						id="login/password"
						label="Contraseña"
						margin="normal"
						fullWidth={true}
						required={true}
						type='password'
						value={form.password ? form.password : ''}
						onChange={this.handleChange('password')}
					/>
					<Button 
						type="submit"
						variant="raised" 
						fullWidth={true} 
						className="btn btn-signin"
						color='primary'
						onClick={() => this.handleOnClick}
					>
						{this.state.loading}
					</Button>
					<button type="submit" id="login" style={{display: 'none'}}>Enviar</button>
				</form>
				{/* <AuthWithGoogle name="Entrar con Google"/>
				<AuthWithFacebook name="Entrar con Facebook"/> */}
			</div>
		)
	}
}

export default Login;