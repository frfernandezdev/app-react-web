import React, { Component } from 'react';
import { Route } from "react-router-dom";

import AppBarCustom from './appbar';
import DrawerLeft from './drawer';

import { 
	Profile,
	Snackbar, 
	SnackbarLoading 
} from '../../utils';

import './index.css';

class LayoutRoot extends Component {
	state = {
		title: null,
		drawer: false,
	}
	constructor(props){
		super()
		this.LoopRoutes = this.LoopRoutes.bind(this)
		this.changeTitle = this.changeTitle.bind(this)
		this.toggleDrawer = this.toggleDrawer.bind(this)
	}
	toggleDrawer = event => {
		this.setState({drawer: event})
	}
	changeTitle = title => {
		this.setState({title: title})
	}
	LoopRoutes = (routes, snackbar, snackbarClose, snackbarLoading, snackbarLoadingClose) => routes.map((route, i) =>
		<Route
			key={i}
			path={route.path}
			exact={route.exact}
			render={props => 
				<route.component 
					{...props} 
					{...this.props} 
					routes={route.routes} 
					title={this.changeTitle}
					snackbar={snackbar} 
					snackbarClose={snackbarClose}
					snackbarLoading={snackbarLoading}
					snackbarLoadingClose={snackbarLoadingClose} 
				/>
			}
		/>
	)
	render(){
		const { routes } = this.props;
		const { title, drawer } = this.state; 
		return (
			<div className="wrapper">
				<AppBarCustom title={title} toggleDrawer={this.toggleDrawer} drawer={drawer} {...this.props} handleOpenAccount={this.state.onClickOpenAccount}/>
				{/* <DrawerLeft toggleDrawer={this.toggleDrawer} drawer={drawer} {...this.props}/> */}
				<Profile handleOpen={event => this.setState({ onClickOpenAccount: event })} handleClose={event => this.setState({ onClickCloseAccount: event })} {...this.props} handleSnackbar={this.state.snackbar} handleSnackbarClose={this.state.snackbarClose}/>
				<main>
					<div className="white-space"></div>
					{
						routes && this.LoopRoutes(
							routes, 
							this.state.snackbar, 
							this.state.snackbarClose,
							this.state.snackbarLoading,
							this.state.snackbarLoadingClose
						)
					}	
				</main>
				<Snackbar handleClick={event => this.setState({ snackbar: event })} handleClose={event => this.setState({ snackbarClose: event })}/>
				<SnackbarLoading handleClick={event => this.setState({ snackbarLoading: event })} handleClose={event => this.setState({ snackbarLoadingClose: event })}></SnackbarLoading>
			</div>
		)
	}
}

export default LayoutRoot;