import React, { Component } from 'react';
import { Link } from "react-router-dom";
import {
	Drawer,
	Divider,
	Icon,
	IconButton,
	List,
	ListItem,
	ListItemIcon,
	ListItemText
} from '@material-ui/core';

class DrawerLeft extends Component {
	render(){
		const { drawer, toggleDrawer, location } = this.props;
		return (
			<Drawer
				variant="permanent"
				open={drawer}
				className={drawer ? "drawer open": "drawer"}
				PaperProps={{
					id: "drawer-paper"
				}}
			>
				<div className="drawer-button">
					<IconButton
						onClick={() => toggleDrawer(drawer ? false: true)}
					>
						<Icon>chevron_left</Icon>
					</IconButton>
				</div>
				<Divider />
				<List>
					<Link to="/" className="link">
						<ListItem button className={ location.pathname === '/' ? 'active' : '' }>
								<ListItemIcon>
									<Icon color="primary">dashboard</Icon>
								</ListItemIcon>
								<ListItemText primary="Tablero"/>
						</ListItem> 
					</Link>
					<Link to="/users" className="link">
						<ListItem button className={ location.pathname.match(/users/g) !== null ? 'active' : '' }>
								<ListItemIcon>
									<Icon color="primary">group</Icon>
								</ListItemIcon>
								<ListItemText primary="Usuarios"/>
						</ListItem> 
					</Link>
					<Link to="/languages" className="link">
						<ListItem button className={ location.pathname.match(/languages/g) !== null ? 'active' : '' }>
								<ListItemIcon>
									<Icon color="primary">sort_by_alpha</Icon>
								</ListItemIcon>
								<ListItemText primary="Lenguajes"/>
						</ListItem> 
					</Link>
					<Link to="/currentMoney" className="link">
						<ListItem button className={ location.pathname.match(/currentMoney/g) !== null ? 'active' : '' }>
								<ListItemIcon>
									<Icon color="primary">euro_symbol</Icon>
								</ListItemIcon>
								<ListItemText primary="currentMoney"/>
						</ListItem> 
					</Link>
				</List>
			</Drawer>
		)
	}
}

export default DrawerLeft;