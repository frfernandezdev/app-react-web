import React, { Component } from 'react';
import { 
	AppBar, 
	Toolbar, 
	Typography, 
	IconButton, 
	Button, 
	Icon, 
	Avatar, 
	Divider,
	Menu,
	MenuItem,
	ListItem,
	ListItemText
} from '@material-ui/core';

class AppBarCustom extends Component {
	state = {
		anchorEl: null,
	}
	constructor(props){
		super()

		console.log(props)
		this.auth 		 = props.firebase.auth();
	}
	handleOpenAccount = () => {
		this.props.handleOpenAccount()
		this.handleClose();
	}
	handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
	}
	handleClose = () => {
    this.setState({ anchorEl: null });
	}
	Logout = () => {
		this.auth.signOut()
			.then(res => {
				this.props.history.push("/")
				console.log(`Ha salido`)
			})
			.catch(err => console.log(`Error: ${err.code} ${err.message}`))
	}
	render(){
		const { 
			title, 
			toggleDrawer, 
			drawer, 
			user 
		} = this.props;
    const { anchorEl } = this.state;
		const open = Boolean(anchorEl);
		return(
			<AppBar position="fixed" className={drawer ? "appbar open": "appbar"}>
				<Toolbar className="appbar-toolbar">
					<IconButton 
						color="inherit" 
						aria-label="Menu"
						className="menu"
						onClick={() => {
							toggleDrawer(drawer ? false: true)
						}}
					>
							<Icon>menu</Icon>
					</IconButton>
					<Typography variant="title" color="inherit" className="appbar-typography">
						{title}
					</Typography>
					<div>
						<Button
							aria-owns={open ? 'open' : null}
							aria-haspopup="true"
							className="appbar-account-menu"
							onClick={this.handleMenu}
							color="inherit"
						>	
							{
								!user.info.photo && !user.info.photoURL ? <Icon>account_circle</Icon> : null 
							}
							{
								!user.info.photo && user.info.photoURL ? <Avatar src={user.info.photoURL} alt={user.info.username} /> : null
							}
							{
								!user.info.photoURL && user.info.photo ? <Avatar src={user.info.photo} alt={user.info.username}/> : null
							}
						</Button>
						<Menu
							id="menu-appbar"
							anchorEl={anchorEl}
							anchorOrigin={{
								vertical: 'top',
								horizontal: 'right',
							}}
							transformOrigin={{
								vertical: 'top',
								horizontal: 'right',
							}}
							open={open}
							onClose={this.handleClose}
							PaperProps={{
								id: 'menu-login'
							}}
						>
							<ListItem button>
								{
									!user.info.photo && !user.info.photoURL ? <Icon>account_circle</Icon> : null 
								}
								{
									!user.info.photo && user.info.photoURL ? <Avatar src={user.info.photoURL} alt={user.info.username} /> : null
								}
								{
									!user.info.photoURL && user.info.photo ? <Avatar src={user.info.photo} alt={user.info.username}/> : null
								}
								<ListItemText 
									disableTypography={true}
									primary={
										<Typography 
											noWrap
											variant="subheading"
										>
											{user.auth.displayName}
										</Typography>
									} 
									secondary={
										<Typography 
											noWrap
											variant="caption"
										>
											{user.auth.email}
										</Typography>
									}
								/>
							</ListItem>
							<Divider/>
							<MenuItem onClick={this.handleOpenAccount}>Mi Cuenta</MenuItem>
							<MenuItem onClick={this.Logout}>Salir</MenuItem>
						</Menu>
					</div>
				</Toolbar>
			</AppBar>
		)
	}
}

export default AppBarCustom;