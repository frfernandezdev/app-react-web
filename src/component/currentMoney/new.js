import React, { Component } from 'react';
import {
	MenuItem,
	TextField,
	Typography,
	Paper,
	Grid,
	Icon,
	IconButton, 
	FormControl,
	Input,
	InputLabel,
	InputAdornment
} from '@material-ui/core';
import * as FontAwesome from 'react-icons/lib/fa';

import {
  SelectLocalities,
	SelectTimeZone,
	SelectCurrentMoney,
	SelectLanguage,
	SelectLanguageNative,
	UploadButton,
	DateNow
} from '../../utils';
class New extends Component {
	state = {
		open: false,
		form: {},
		showpassword: false
	}
	constructor(props){
		super(props);

		this.firestore = props.firebase.firestore().collection('currentMoney');
		this.storage   = props.firebase.storage();
		this.onSubmit  = this.onSubmit.bind(this)
		this.handleChange  = this.handleChange.bind(this)
		this.handleOnClick = this.handleOnClick.bind(this)
		this.props.handleOnClick(this.handleOnClick)
	}
	handleChange = name => event => {
		this.setState({
			form: {
				...this.state.form,
				[name]: event.target.value
			}
		})
	}
	onSubmit = event => {
		event.preventDefault();
		this.props.handleOnLoading('show')
		fetch(
			'https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/money',
			{
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'POST',
				mode: 'cors',
				body: JSON.stringify({
					token: this.props.user.auth.token, 
					...this.state.form
				})
			}
		)
		.then(response => response.json())
		.then(response => {
			this.props.handleOnLoading('hide')
			if(response.success === true){
				this.props.handleSnackbar({type: 'custom', message: response.mes})
				setTimeout(() => {
					this.setState({ 
						form: {
							tools: {}
						} 
					})
					this.props.handleOnLoading('hide')
				}, 2500)
				this.props.handleOnLoading('done')
			}
			else {
				this.props.handleOnLoading('hide')
				if(response.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.error, on: true })
			}
		})
		.catch(error => {
			this.props.handleOnLoading('hide')
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
	}
	handleOnClick = () => {
	document.getElementById('new/submit').click()
	}
	render() {
			const { id, value } = this.props;
			const { showpassword } = this.state;
			return (
				<Paper style={{ padding: 15 }}>
						<form 
								id="new"
								onSubmit={this.onSubmit} 
								style={{ display: value === id ? 'block' : 'none' }}
						>
							<Typography variant="title" gutterBottom>Moneda</Typography>
							<Grid container>
										<Grid
												item 
												xs={12}
												sm={12}
												md={12}
												lg={12}
												xl={12}
												style={{
													paddingLeft: 10,
													paddingRight: 10
												}}
										>
									</Grid>
									<Grid 
										item 
										xs={12} 
										sm={12} 
										md={6} 
										lg={6} 
										xl={6} 
										style={{
											paddingLeft: 10,
											paddingRight: 10
										}}
									>
										<TextField
											id='modify/name'
											label="Nombre"
											margin="normal"
											fullWidth
											// required
											// error={this.state.error.username}
											type='text'
											value={ 
												this.state.form.name 
													? 
												this.state.form.name 
													: 
												''
											}
											onChange={this.handleChange('name')}
										/>
											
									</Grid>
									<Grid 
										item 
										xs={12} 
										sm={12} 
										md={6} 
										lg={6} 
										xl={6} 
										style={{
											paddingLeft: 10,
											paddingRight: 10
										}}
									>
										<TextField
											id="modify/acronym"
											label="Acronymo"
											margin="normal"
											fullWidth
											// required
											// error={this.state.error.email}
											value={
												this.state.form.acronym 
													? 
												this.state.form.acronym 
													: 
												''
											}
											type='text'
											onChange={this.handleChange('acronym')}
										/>
									</Grid>	
									<button type="submit" id="new/submit" style={{display: 'none'}}>Enviar</button>	
								</Grid>		
						</form>
					</Paper>
        )
    }
}   
 export default New;