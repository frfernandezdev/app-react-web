import React, { Component } from 'react';
import { Paper, Tooltip } from '@material-ui/core';

import { TableCustom } from '../../utils';

const headCollection = [
	{
		title: '#', 
		tooltip: false
	},
	{
		title: 'Acronimos',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
	},
	{
		title: 'Nombre',
		tooltip: true,
		position: 'bottom-end',
		delay: 300
	}
]

class List extends Component {
	state = {
		currentMoney: []
	}
	constructor(props){
		super()

		this.firestore = props.firebase.firestore().collection('currentMoney');
	}
	componentWillMount(){
		this.firestore
			.onSnapshot(snapshot => {
				this.setState({ currentMoney: snapshot.docs })
			}, error => console.log(error))
	}
	render(){
		const { currentMoney } = this.state;
		const { 
			handleChange, 
			id, 
			value 
		} = this.props;
		return (
			<Paper style={{
				width: '100%',
				overflowX: 'auto',
			}}>
				<div style={{ display: value === id ? 'block' : 'none' }}>
					<TableCustom
						headCollection={headCollection}
						bodyCollection={currentMoney}
						handleEdit={handleChange}
						handleDelete={this.props.handleDelete}
						handleView={this.props.handleView}
						// handleView={() => console.log('here')}
						footer=""
						style={{
							minWidth: 700,
						}}
						template="languagesCurrentMoney"
					/>
				</div>
			</Paper>
		)
	}
}

export default List;