import React, { Component } from 'react';
import {
	colors, 
	Typography, 
	Button,
	Icon,
	CircularProgress,
	IconButton,
	BottomNavigation,
	BottomNavigationAction } from '@material-ui/core';
import green from '@material-ui/core/colors/green';
import SwipeableViews from 'react-swipeable-views';

import List from './list';
import New from './new';
import Modify from './modify';
import View from './view';

import { DialogDelete } from '../../utils';

import './index.css';

class currentMoney extends Component {
	state = {
		users:[],
		value: 0,
		disabled: true,
		onclick: false,
		show: '' 
	}
	constructor(props){
		super()
		props.title('Moneda')

		this.onClick = this.onClick.bind(this)
		this.handleCange = this.handleChange.bind(this)
	}
	componentWillMount(){
		switch(this.props.location.pathname){
			case '/currentMoney/new':
				this.handleChange(null, 1)
				break;
			case '/currentMoney/edit':
				if(
            this.props.location.state !== undefined 
              &&
            'id' in this.props.location.state 
              &&
            this.props.location.state.id !== undefined 
          )
					this.handleChange(null, 2)
				else
					this.handleChange(null, 0)
				break;
			default:
				this.handleChange(null, 0)            
		}
	}

	componentDidMount(){
		document.title = 'Moneda - SpeakEnglishSite'
		window.addEventListener('scroll', this.handleScroll);
	}
	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll);
	}
	handleChange = (event, value, uid) => {
		console.log(event, value, uid)
		this.setState(
			{
				value,
				disabled: this.state.value === 2 || value === 2 ? false : true,
				uid
			},  () => {
				switch(this.state.value){
					case 1:
						this.props.history.push('/currentMoney/new')
						break;
					case 2:
						this.props.history.push('/currentMoney/edit',{id: uid})
						break;
					default:
						this.props.history.push('/currentMoney')
				}
			}
		)
	}
	handleScroll = (event) => {
		if(document.documentElement.scrollTop > 160)
			this.setState({show: 'show'})
		else
			this.setState({ show: '' })
	}
	handleOnLoading = action => {
		switch(action){
			case 'show':
				this.setState({ loading: true })
				break;
			case 'done':
				this.setState({ loading: false, done: true })
				break;
			case 'hide':
				this.setState({ loading: false, done: null })
				break;
		} 
	}
	onClick = event => {
		switch(this.state.value){
			case 0:
				this.setState({ value: 1 })
				break;
			case 1:
				this.state.onclickNew()
				break;
			case 2:
				this.state.onclickModify()
		}
	}
	render(){
		const { value, disabled, loading, done } = this.state;
		return (
			<div>
			<div className="header" style={{background: colors.indigo[700]}}>
				<Typography
					style={{color: 'white'}}
					variant="title"
				>
					Moneda
				</Typography>
				<div className={`btn-fab top ${this.state.show}`}>
					<Button
						id="fab"
						variant="fab"
						color="default"
						onClick={this.onClick}
						style={
							done
								&&
							{
								backgroundColor: green[500],
								'&:hover': {
									backgroundColor: green[700],
								}
							}
						}
					>
						{
							value === 0
								?
							<Icon>add</Icon>
								:
							value === 1 || value === 2
								?
							<Icon>done</Icon>
								:
							done
								?
							<Icon>done_all</Icon>
								:
							null
						} 
					</Button>
					{
						loading 
						&&
					<CircularProgress size={68} style={{color: green[500]}} className="circle-progress" />   
					}
				</div>
			</div>
			<BottomNavigation
				value={value}
				onChange={this.handleChange}
				style={{
					boxShadow: `
						0px 1px 5px 0px rgba(0, 0, 0, 0.2), 
						0px 2px 2px 0px rgba(0, 0, 0, 0.14), 
						0px 3px 1px -2px rgba(0, 0, 0, 0.12)`
				}}
			>
				<BottomNavigationAction 
						label="Lista"
						icon={
							<Icon>view_list</Icon>
						} 
					/>
					<BottomNavigationAction 
						label="Nuevo" 
						icon={
							<Icon>add_box</Icon>
						} 
					/>
					<BottomNavigationAction
						label="Editar"
						disabled={disabled}
						icon={
							<Icon>edit</Icon>		
						}
					/>
					</BottomNavigation> 	
				<div className="content">
					<div style={{padding: '15px 15px'}}>
							<SwipeableViews
								index={value}
								slideStyle={{
									alignItems: 'inherit',
									display: 'initial',
									padding: 5
								}}
							>
									<List 
										handleChange={this.handleChange} 
										handleView={(event, uid) => {} }
										handleDelete={(event, uid) => this.state.onclickDelete(event,uid)}   
										id={0} 
										value={value} 
										{...this.props}
									/>
									<New 
										id={1} 
										value={value} 
										handleOnClick={event => this.setState({ onclickNew: event })} 
										handleSnackbar={this.props.snackbar} 
										handleSnackbarClose={this.props.snackbarClose} 
										handleOnLoading={this.handleOnLoading} 
										{...this.props}
									/>
									<Modify 
										handleChange={this.handleChange} 
										id={2} 
										value={value} 
										handleOnClick={event => this.setState({ onclickModify: event })} 
										handleSnackbar={this.props.snackbar} 
										handleSnackbarClose={this.props.snackbarClose} 
										handleOnLoading={this.handleOnLoading} 
										{...this.props}
									/>
							</SwipeableViews>
							</div>
				</div>
				<div className={`btn-fab bottom ${this.state.show}`}>
					<Button
						id="fab"
						variant="fab"
						color="default"
						style={
							done
								&&
							{
								backgroundColor: green[500],
    						'&:hover': {
      						backgroundColor: green[700],
    						}
							}
						}
						onClick={this.onClick}
					>
						{
							value === 0 
								?
							<Icon>add</Icon>
								:
							value === 1 || value === 2
								?
							<Icon>done</Icon>
								:
							done 
								? 
							<Icon>done_all</Icon>
								:
							null	
						}
					</Button>
					{ 
						loading 
							&&
						<CircularProgress size={68} style={{color: green[500]}} className="circle-progress" />
					}
				</div>
				{/* <View 
					handleOpen={event => this.setState({ onclickOpenView : event })} 
					handleClose={event => this.setState({ onclickCloseView: event })}
					handleSnackbar={this.props.snackbar} 
					handleSnackbarClose={this.props.snackbarClose}
					{...this.props}
				/> */}
				<DialogDelete
					handleOpen={event => this.setState({ onclickDelete: event })} // Evento para abrir el modal de eliminacion
					label="Moneda" // Label es texto que va estar situado en el header el modal de eliminacion
					url="money" // url.
					handleSnackbar={this.props.snackbar} // manejador de snackbar
					handleSnackbarClose={this.props.snackbarClose} // manejador de snackbar
					{...this.props}
				/>
			</div>

		)
	}
}

export default currentMoney;