import React, { Component } from 'react';
import { Route } from "react-router-dom";

import { Snackbar } from '../../utils';

import './index.css';

class Layout extends Component {
	state = {}
	constructor(props){
    super()
    
		this.LoopRoutes = this.LoopRoutes.bind(this);
	}
	LoopRoutes = (routes, snackbar, snackbarClose) => routes.map((route, i) =>
		<Route
			key={i}
			path={route.path}
			exact={route.exact}
			render={props => 
        <route.component 
          {...props} 
					{...this.props} 
					snackbar={snackbar} 
					snackbarClose={snackbarClose}
          routes={route.routes} 
        />
			}
		/>
	)
	render(){
		const { routes } = this.props;
		const { title, drawer } = this.state;
		return (
			<div className="wrapper-page">
				<main className="main">
					{
						routes && this.LoopRoutes(routes, this.state.snackbar, this.state.snackbarClose)
					}	
				</main>
				<Snackbar handleClick={event => this.setState({ snackbar: event })} handleClose={event => this.setState({ snackbarClose: event })}/>
			</div>
		)
	}
}

export default Layout;