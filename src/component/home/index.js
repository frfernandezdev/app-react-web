import React, { Component } from 'react';
import { Calendar } from '../../utils';
import './index.css';

const schedule = [
  {
    date: '2018-6-20 00:00',
    booking: true,
  },
  {
    date: '2018-6-20 23:00',
    booking: false,
  },
  {
    date: '2018-6-20 12:00',
    booking: false,
  },
  {
    date: '2018-6-17 00:00',
    booking: true,
  },
  {
    date: '2018-6-17 05:00',
    booking: false,
  },
  {
    date: '2018-6-17 10:00',
    booking: true,
  },
  {
    date: '2018-6-17 13:00',
    booking: false,
  },
  {
    date: '2018-6-17 19:00',
    booking: true,
  }
]

class Home extends Component {
	state = { 
		events: [
			{
				id: 0,
				start: new Date(),
				end: new Date(),
			}
		] 
	}
	constructor(props){
		super()
		props.title('Tablero')
	}
	componentDidMount(){
		document.title = 'Tablero - SpeakEnglishSite'
	}
	render(){
		const { events } = this.state;
		return (
			<div>
				<h1>{this.props.user.auth.displayName}</h1>
				<span>{this.props.user.auth.email}</span>
				<Calendar
					schedule={schedule} 
					// seletedAll={true}
					// limit={2}
					onChange={ event => console.log(event) }
				/>
			</div>
		)
	}
}

export default Home;