import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import Routers from './router';
import firebase from './firebase';

import './index.css';

ReactDOM.render(<Routers firebase={firebase} />, document.getElementById('root'));
registerServiceWorker();
