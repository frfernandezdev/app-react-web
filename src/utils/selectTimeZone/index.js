import React, { Component } from 'react';
import {
	TextField,
	MenuItem
} from '@material-ui/core';

import timezone from './timezone';

class SelectTimeZone extends Component {
	state = {
		timezone: null
	}
	componentWillUpdate(prevProps, prevState, snapshot){
		if('value' in prevProps){
			if(prevProps.value.text !== prevState.timezone){
				this.setState({ timezone: prevProps.value.text })
			}
		}
	}
	handleFind = event => {
		this.props.onChange({
      target: {
        value: timezone.find(el => el.text === event.target.value)
      }
    })
	}
	handleChange = name => event => {
		this.setState({
			[name]: event.target.value
		})
    this.handleFind(event)
	}
	render(){
		const { label, id, disabled, className, required } = this.props;
		return (
			<TextField
				select
				label={label}
				required={required}
				disabled={disabled}
				className={className}
				value={this.state.timezone ? this.state.timezone : ''}
				onChange={this.handleChange('timezone')}
				fullWidth
				margin="normal"
			>
				{
					timezone.map((docs, key) => 
						<MenuItem key={key} value={docs.text}>
							{docs.text}
						</MenuItem>
					)
				}
			</TextField>
		)
	}
}

export default SelectTimeZone;