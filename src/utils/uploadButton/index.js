import React,{ Component } from 'react';
import {
	Button,
	Avatar,
	Icon,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle
} from '@material-ui/core';

import './index.css';

class UploadButton extends Component {
	state = {
		open: false,
		img : null,
		src: null,
		file: null,
		loader: false,
		success: false
	}
	componentWillUpdate(prevProps, prevState, snapshot){
		if('delete' in prevProps){
			if(prevProps.delete === true && prevState.file !== null && prevState.img !== null){
				this.deleteForm();
			}
		}
		if('value' in prevProps){
			if(prevProps.value !== "" && prevState.file === null && prevState.img === null){
				this.valueForm(prevProps.value);
			}
		}
	}
	deleteForm = event => {
		this.state = {
			open: false,
			img : null,
			file: null,
			loader: false,
			success: false
		}
	}
	valueForm = event => {
		this.state.img = event;
		this.state.src = event;
	}
	handleChange = event => {
		if(event.target.files && event.target.files[0]){
			let reader = new FileReader();
			reader.onloadstart = () => {
				this.setState({ loader : true })
			}
			reader.onload = e => {
				this.setState({ img : e.target.result})
			}
			reader.onloadend = () => {
				this.setState({ loader: false, success: true })
			}
			reader.readAsDataURL(event.target.files[0])
			this.props.onChange({
				target: {
					value: event.target.files[0]
				}
			})
			this.setState({ file: event.target.files[0] })
		}
	}
	handleRemove = () => {
		if(this.state.src)
			this.setState({ 
				loader : false,
				img: this.state.src,
				file: null,
				success: false 
			})
		else 
			this.setState({ 
				loader : false,
				img: null,
				file: null,
				success: false 
			})
	}
	handleClickOpen = () => {
		this.setState({ open : true })
	}
	handleClose = () => {
		this.handleRemove()
		this.setState({ open : false })
	}
	render() {
		const { 
			open, 
			img, 
			success 
		} = this.state;
		const { 
			labelButton, 
			labelDialog, 
			labelButtonSuccess, 
			disabled,
			style
		} = this.props;
		return (
			<div
				style={style}
			>
				{
					!disabled
						&&
					<Button 
						onClick={this.handleClickOpen} 
						variant="raised" 
						color="default"
						fullWidth
					>
						{
							img 
								?
							<Avatar src={img} style={{ marginRight: 5 }}/>
								:
							<Icon>file_upload</Icon>	
						}
						{
							img 
								? 
							labelButtonSuccess 
								: 
							labelButton
						}
					</Button>
				}
				<Dialog
					open={open}
					onClose={this.handleClose}
					aria-labelledby="responsive-dialog-title"
					fullWidth
				>
					<DialogTitle id="responsive-dialog-title">{labelDialog}</DialogTitle>
					<DialogContent className={'dialog-upload-content'}>
						{
							img
								? 
							<img src={img}/>
								:
							''
						}
						<div>
							<input 
								type="file" 
								accept="image/*"
								className="inputFileUpload"
								id="inputFileUpload"
								onChange={this.handleChange}
							/>
							{
								success
									?
								<Button
									variant="fab"
									component="span"
									color="secondary"
									onClick={this.handleRemove}
								>
									<Icon>remove</Icon>
								</Button>
									:
								<label htmlFor="inputFileUpload">
									<Button
										variant="fab"
										component="span"
										color="primary"
									>
										<Icon>add_to_photos</Icon>
									</Button>
								</label>
							}
						</div>
					</DialogContent>
					<DialogActions>
						<Button onClick={this.handleClose}>Cancelar</Button>
						<Button onClick={event => this.setState({ open : false })}>Guardar</Button>
					</DialogActions>
				</Dialog>
			</div>
		)
	}
}

export default UploadButton;