import React,{ Component } from 'react';
import {
	TextField,
	MenuItem,
} from '@material-ui/core';

class SelectCurrentMoney extends Component {
	state = {
		money: '',
		object: []
	}
	constructor(props){
		super(props);

		this.firestore = props.firebase.firestore().collection('currentMoney');
	}
	componentWillMount(){
		this.firestore
			.orderBy('acronym')
			.onSnapshot(snapshot => {
				this.setState({ object: snapshot.docs })
			}, error => {
				console.log(error)
			})
	}
	componentWillUpdate(prevProps, prevState, snapshot){
		if('delete' in prevProps){
			if(prevProps.delete === true && prevState.money !== ''){
				this.deleteForm();
			}
		}
		if('value' in prevProps){
			if(prevProps.value !== "" && prevState.money === ''){
				this.valueForm(prevProps.value);
			}
		}
	}
	deleteForm = event => {
		this.state.money = '';
	}
	valueForm = event => {
		this.state.money = event
	}	
	handleChange = name => event => {
		let object = {
			target: {
				value: event.target.value
			}
		}
		this.props.onChange(object)
		this.setState({
			[name]: event.target.value
		})
	}
	render() {
		const { object } = this.state;
		const { label, disabled, className, required } = this.props;
		return (
			<TextField
				select
				label={label}
				required={required}
				value={this.state.money ? this.state.money : ''}
				onChange={this.handleChange('money')}
				fullWidth
				disabled={disabled}
				className={className}
				margin="normal"
			>
				{
					object.length > 0
						?
					object.map(docs => {
						let collection = docs.data();
						return (
							<MenuItem key={docs.id} value={docs.id}>
								{collection.acronym}
							</MenuItem>
						)
					}) 
						:
					<MenuItem value="">No hay Monedas</MenuItem>
				}
			</TextField>
		)
	}
}

export default SelectCurrentMoney;