import React,{ Component } from 'react';
import {
	Paper,
	Typography,
	Divider,
	Grid,
	TextField,
	FormControl,
	InputLabel,
	Input,
	Card,
	CardContent,
	CardActions,
	CardMedia,
	CardHeader,
	Button,
	Avatar,
	CircularProgress,
	InputAdornment,
	IconButton,
	Icon
} from '@material-ui/core';
import Google from 'react-icons/lib/fa/google';
import Facebook from 'react-icons/lib/fa/facebook-official';

class Credentials extends Component {
	state = { 
		showpassword: false, 
		disabled: true, 
		form: {}, 
		google: null, 
		facebook: null,
		loadingFacebook: false,
		loadingGoogle: false
	}
	constructor(props){
		super(props)

		this.handleOnClick = this.handleOnClick.bind(this)
		this.handleOnSubmit = this.handleOnSubmit.bind(this)
		this.props.handleOnEnabled(() => this.setState({ disabled: false }))
		this.props.handleOnSubmit(this.handleOnClick)
		this.props.handleCancel(() => this.setState({ disabled: true, form: {} }))

		this.linkAccounts 	= this.linkAccounts.bind(this)
		this.unlinkAccounts = this.unlinkAccounts.bind(this)

		this.auth = this.props.firebase.auth;

		this.providerGoogle = new this.auth.GoogleAuthProvider();
		this.providerFacebook = new this.auth.FacebookAuthProvider();
	}
	handleOnSubmit = event => {
		event.preventDefault();
		this.props.handleLoading()
		console.log(this.state.form)
		fetch(
			'https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/auth',
			{
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'PUT',
				mode: 'cors',
				body: JSON.stringify({
					token: this.props.user.auth.token,
					...this.state.form
				})
			}
		)
		.then(response => response.json())
		.then(response => {
			if(response.success === true){
				this.setState({ disabled: true, form: {} })
				this.props.handleReset()
				this.props.handleSnackbar({type: 'custom', message: response.mes}) 
			}else {
				this.setState({ disabled: true, form: {} })
				this.props.handleReset()
				if(response.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			this.props.handleReset()
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
	}
	handleChange = name => event => {
		if(this.state.form[name] === this.props.user.info[name])
			return;	
		this.setState({
			form: {
				...this.state.form,
				[name]: event.target.value
			}
		})
	}
	load = (providerId) => {
		switch(providerId){
			case 'google.com':
				this.setState({ loadingGoogle: this.state.loadingGoogle });
				return this.providerGoogle;
			case 'facebook.com':
				this.setState({ loadingFacebook: this.state.loadingFacebook });
				return this.providerFacebook;
		}
	}
	linkAccounts = (providerId) => {
		let provider = this.load(providerId);
		this.auth().currentUser.linkWithPopup(provider)
			.then(result => {
				console.log('succes link')
				if(result){
					this.props.reloadInfo();
					this.load(providerId);
				}
			})
			.catch(error => console.log(error));
		console.log('link')
	}
	unlinkAccounts = (providerId) => {
		this.load(providerId)
		this.auth().currentUser.unlink(providerId)
			.then(result => {
				if(result){
					this.props.reloadInfo();
					this.load(providerId);
				}
			})
			.catch(error => console.log(error))
	}
	handleChange = name => event => {
		if(this.state.form[name] === this.props.user.info[name])
			return;	
		this.setState({
			form: {
				...this.state.form,
				[name]: event.target.value
			}
		})
	}
	handleOnClick = () => {
		document.getElementById('profile/credentials').click();
	}
	render() {
		const { page, user } = this.props;
		const { showpassword, disabled, loadingFacebook, loadingGoogle, form } = this.state;
		const google 	 = user.auth.providerData.find(docs => docs.providerId === 'google.com' ? docs : null);
		const facebook = user.auth.providerData.find(docs => docs.providerId === 'facebook.com' ? docs : null);
		return (
			<div style={{ 
				padding: 15,
				display: 'flex',
				position: 'relative',
				flexDirection: 'column' 
			}}>
				<div style={{ display: page === 1 ? 'block' : 'none' }}>
					<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
						<Typography variant="title" gutterBottom>Credenciales</Typography>
						<Divider/>
						<form onSubmit={this.handleOnSubmit}>
							<Grid container spacing={8}>
								<Grid 
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
								>
									<TextField
										id='view/username'
										label="Nombre de usuario"
										margin="normal"
										fullWidth
										disabled={disabled}
										className="input-view"
										type='text'
										value={
											form.hasOwnProperty('username')
												?
											form.username
												:
											user 
												? 
											user.info.username 
												: 
											''
										}
										onChange={this.handleChange('username')}
									/>
									<TextField
										id="view/mail"
										label="Correo electronico"
										margin="normal"
										disabled={disabled}
										className="input-view"
										fullWidth
										type='email'
										value={
											form.hasOwnProperty('email')
												?
											form.email
												:
											user 
												? 
											user.info.email 
												: 
											''
										}
										onChange={this.handleChange('email')}
									/>
								</Grid>
								<Grid 
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
								>
									<TextField
										id="view/mobile"
										label="Telefono movil"
										placeholder="+581234567890"
										margin="normal"
										fullWidth
										disabled={disabled} 
										className="input-view"
										type='phone'
										inputProps={{
											pattern: '[+][0-9]{12}',
											title: 'Solo se aceptan letras'
										}}
										value={
											form.hasOwnProperty('mobile')
												?
											form.mobile
												:
											user 
												? 
											user.info.mobile 
												: 
											''
										}
										onChange={this.handleChange('mobile')}
									/>
									<FormControl
										margin="normal"
										fullWidth
										disabled={disabled}
										className="input-view"
									>
										<InputLabel htmlFor="password">Contraseña</InputLabel> 	
										<Input
											id="view/password"
											label="Contraseña"
											value={
												form.hasOwnProperty('password')
													?
												form.password
													:
												'12345678'
											}
											type={ showpassword ? 'text' : 'password' }
											endAdornment={
												!disabled
													&&
												<InputAdornment position="end">
													<IconButton
														aria-label="Toggle password visibility"
														disabled
														onClick={
															event => this.setState((prevState, props) => {
																return {showpassword: !prevState.showpassword}
															}
														)}
														onMouseDown={event => event.preventDefault()}
													>
														<Icon>{ showpassword ? 'visibility' : 'visibility_off' }</Icon>
													</IconButton>
												</InputAdornment>
											}
											onChange={this.handleChange('password')}
										/>
									</FormControl>
								</Grid>
							</Grid>
							<button type="submit" id="profile/credentials" style={{display: 'none'}}>Enviar</button>
						</form>
					</Paper>
					<Paper style={{ marginTop: 10, padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
						<Typography variant="title" gutterBottom>Cuentas Vinculadas</Typography>
						<Divider/>
						<Grid container spacing={16} style={{ padding: '15px 0' }}>
							{
								user.auth.providerData.map(docs => {
									if(docs.providerId !== 'password' && docs.providerId !== 'phone')
										return (
											<Grid
												key={docs.providerId}
												item
												xs={12}
												sm={12}
												md={6}
												lg={6}
												xl={6}
											>
												<Card>
													<CardHeader
														avatar={
															<Avatar style={{ border: '1px solid #eee', backgroundColor: 'transparent' }}>
																{
																	docs.providerId === 'google.com' 
																		?
																	<Google className="google"/>
																		:		
																	docs.providerId === 'facebook.com'
																		?
																	<Facebook className="facebook"/>
																		:
																	''
																}
															</Avatar>
														}
														title={
															docs.providerId === 'google.com' 
																?
															"Google Proveedor"
																:		
															docs.providerId === 'facebook.com'
																?
															"Facebook Proveedor"
																:
															''
														}
													/>
													<CardMedia
														style={{ height: 0, paddingTop: '56.25%' }}
														image={docs.photoURL}
														title={docs.displayName}
													/>
													<CardContent>
														<Typography gutterBottom variant="headline" component="h4">
															{docs.displayName}
														</Typography>
														<Typography component="p">{`Correo Electronico: ${docs.email}`}</Typography>
														<Typography component="p">{`ID Proveedor: ${docs.uid}`}</Typography>
													</CardContent>
													<CardActions>
														<Button
															variant="raised"
															fullWidth
															style={{ backgroundColor: '#fff' }}
															onClick={() => this.unlinkAccounts(docs.providerId)}
														>
															{
																docs.providerId === 'google.com' && loadingGoogle === true
																	?
																<CircularProgress size={20} color='inherit'/>
																	:
																docs.providerId === 'facebook.com' && loadingFacebook === true
																	?
																<CircularProgress size={20} color='inherit'/>
																	:
																<span> Desvincular </span>
															}
														</Button>
													</CardActions>
												</Card>
											</Grid>
										)
								})	
							}
							{
								!google
									&&
								<Grid
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
								>
									<Button
										variant="raised"
										fullWidth
										style={{ backgroundColor: '#fff' }}
										onClick={() => this.linkAccounts('google.com')}
									>
										{
											loadingGoogle === true
												? 
											<CircularProgress size={20} color='inherit' />
												:
											<div>
												<Google size={26} className="google icon-button"/>
												Google
											</div>
										}
									</Button>
								</Grid>
							}
							{
								!facebook
									&&
								<Grid
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
								>
									<Button
										variant="raised"
										fullWidth
										style={{ backgroundColor: '#fff' }}
										onClick={() => this.linkAccounts('facebook.com')}
									>
										{
											loadingFacebook === true
												?
											<CircularProgress size={20} color='inherit' />
												:
											<div>
												<Facebook size={26} className="facebook icon-button"/>
												Facebook
											</div>
										}
									</Button>
								</Grid>
							}
						</Grid>
					</Paper>
				</div>
			</div>
		)
	}
}

export default Credentials;