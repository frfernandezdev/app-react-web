import React, { Component } from 'react';
import {
	Paper,
	Typography,
	Divider,
	Grid,
	TextField,
	InputAdornment,
	Icon
} from '@material-ui/core';
import * as FontAwesome from 'react-icons/lib/fa';

class ToolsComunications extends Component {
	state = { form: { tools: {} }, disabled: true }
	constructor(props){
		super(props)

		this.handleOnClick = this.handleOnClick.bind(this)
		this.handleOnSubmit = this.handleOnSubmit.bind(this)
		this.props.handleOnEnabled(() => this.setState({ disabled: false }))
		this.props.handleOnSubmit(this.handleOnClick)
		this.props.handleCancel(() => this.setState({ disabled: true, form: {} }))
	}
	handleOnSubmit = event => {
		event.preventDefault();
		this.props.handleLoading()
		console.log(this.state.form)
		fetch(
			'https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/auth',
			{
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'PUT',
				mode: 'cors',
				body: JSON.stringify({
					token: this.props.user.auth.token,
					...this.state.form
				})
			}
		)
		.then(response => response.json())
		.then(response => {
			if(response.success === true){
				this.setState({ disabled: true, form: {} })
				this.props.handleReset()
				this.props.handleSnackbar({type: 'custom', message: response.mes}) 
			}else {
				this.setState({ disabled: true, form: {} })
				this.props.handleReset()
				if(response.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			this.props.handleReset()
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
	}
	handleChange = name => event => {
		if([name].indexOf(this.props.user.info.tools) !== -1 && this.state.form.tools[name] === this.props.user.info.tools[name])
			return;
		this.setState({
			form: {
				tools: {
					...this.state.form.tools,
					[name]: event.target.value
				}
			}	
		})
	}
	handleOnClick = () => {
		document.getElementById('profile/tools').click()
	}
	render() {
		const { page, user } = this.props;
		const { form, disabled } = this.state;
		return (
			<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
				<div style={{ display: page === 3 ? 'block' : 'none' }}>
					<Typography variant="title" gutterBottom>Herramientas de Comunicación</Typography>
					<Divider/>
					<form onSubmit={this.handleOnSubmit}>
						<Grid container spacing={8}>
							<Grid 
								item
								xs={12}
								sm={12}
								md={6}
								lg={6}
								xl={6}
							>
							<TextField
								id="modify/tool-skype"
								label="Skype"
								margin="normal"
								fullWidth
								disabled={disabled}
								className={disabled ? "input-view" : ""}
								type="email"
								value={
									form.hasOwnProperty('tools')
										?
									form.tools.hasOwnProperty('skype')
										:
									user.info.tools.skype	 
										? 
									user.info.tools.skype 
										: 
									''
								}
								InputProps={{
									startAdornment: (
										<InputAdornment position="start">
											<FontAwesome.FaSkype/>
										</InputAdornment>
									)
								}}
								onChange={this.handleChange('skype')}
							/>
							<TextField
								id="modify/tool-facetime"
								label="FaceTime"
								margin="normal"
								fullWidth
								disabled={disabled}
								className={disabled ? "input-view" : ""}
								type="email"
								value={
									form.hasOwnProperty('tools')
										?
									form.tools.hasOwnProperty('facetime')
										:
									user.info.tools.facetime	 
										? 
									user.info.tools.facetime 
										: 
									''
								}
								InputProps={{
									startAdornment: (
										<InputAdornment position="start">
											<Icon>videocam</Icon>
										</InputAdornment>
									)
								}}
								onChange={this.handleChange('facetime')}
							/>
							<TextField
								id="modify/tool-qqhangouts"
								label="Google Hangouts"
								margin="normal"
								fullWidth
								disabled={disabled}
								className={disabled ? "input-view" : ""}
								type="email"
								value={
									form.hasOwnProperty('tools')
										?
									form.tools.hasOwnProperty('gHangouts')
										:
									user.info.tools.gHangouts	 
										? 
									user.info.tools.gHangouts
										: 
									''
								}
								InputProps={{
									startAdornment: (
										<InputAdornment position="start">
											<Icon>format_quote</Icon>
										</InputAdornment>
									)
								}}
								onChange={this.handleChange('gHangouts')}
							/>
							</Grid>
							<Grid 
								item
								xs={12}
								sm={12}
								md={6}
								lg={6}
								xl={6}
							>
							<TextField
								id="modify/tool-wechat"
								label="WeChat"
								margin="normal"
								fullWidth
								disabled={disabled}
								className={disabled ? "input-view" : ""}
								type="email"
								value={
									form.hasOwnProperty('tools')	
										?
									form.tools.hasOwnProperty('wechat')
										:
									user.info.tools.wechat 
										? 
									user.info.tools.wechat 
										: 
									''
								}
								InputProps={{
									startAdornment: (
										<InputAdornment position="start">
											<FontAwesome.FaWechat/>
										</InputAdornment>
									)
								}}
								onChange={this.handleChange('wechat')}
							/>
							<TextField
								id="modify/tool-qq"
								label="Tencent QQ"
								margin="normal"
								fullWidth
								disabled={disabled}
								className={disabled ? "input-view" : ""}
								type="email"
								value={
									form.hasOwnProperty('tools')
										?
									form.tools.hasOwnProperty('qq')	
										:
									user.info.tools.qq	
										? 
									user.info.tools.qq 
										: 
									''
								}
								InputProps={{
									startAdornment: (
										<InputAdornment position="start">
											<FontAwesome.FaQq/>
										</InputAdornment>
									)
								}}
								onChange={this.handleChange('qq')}
							/>
							</Grid>
						</Grid>
						<button type="submit" id="profile/tools" style={{display: 'none'}}>Enviar</button>
					</form>
				</div>
			</Paper>
		)
	}
}

export default ToolsComunications;