import React,{ Component } from 'react';
import {
	Paper,
	Typography,
	Grid,
	Divider
} from '@material-ui/core';
import {
	SelectLanguage,
	SelectLanguageNative
} from '../../utils';

class Language extends Component {
	state = { form: {}, disabled: true }
	constructor(props){
		super(props)

		this.handleOnClick = this.handleOnClick.bind(this)
		this.handleOnSubmit = this.handleOnSubmit.bind(this)
		this.props.handleOnEnabled(() => this.setState({ disabled: false }))
		this.props.handleOnSubmit(this.handleOnClick)
		this.props.handleCancel(() => this.setState({ disabled: true, form: {} }))
	}
	handleOnSubmit = event => {
		event.preventDefault();
		this.props.handleLoading()
		console.log(this.state.form)
		fetch(
			'https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/auth',
			{
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'PUT',
				mode: 'cors',
				body: JSON.stringify({
					token: this.props.user.auth.token,
					...this.state.form
				})
			}
		)
		.then(response => response.json())
		.then(response => {
			if(response.success === true){
				this.setState({ disabled: true, form: {} })
				this.props.handleReset()
				this.props.handleSnackbar({type: 'custom', message: response.mes}) 
			}else {
				this.setState({ disabled: true, form: {} })
				this.props.handleReset()
				if(response.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			this.props.handleReset()
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
	}
	handleChange = name => event => {
		if(this.state.form[name] === this.props.user.info[name])
			return;	
		this.setState({
			form: {
				...this.state.form,
				[name]: event.target.value
			}
		})
	}
	handleOnClick = () => {
		document.getElementById('profile/language').click()
	}
	render(){
		const { user, page } = this.props;
		const { form, disabled } = this.state;
		return (
			<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
				<div style={{ display: page === 2 ? 'block' : 'none' }}>
					<Typography variant="title" gutterBottom>Idiomas</Typography>
					<Divider/>
					<form onSubmit={this.handleOnSubmit}>
						<Grid container spacing={8}>
							<Grid 
								item
								xs={12}
							>
								<Typography style={{ margin: '10px 0px' }}>Idiomas Nativo</Typography>
								<SelectLanguageNative 
									id="view/languageNative"
									limit={2}
									disabled={disabled}
									className={disabled ? "input-view" : ""}
									value={
										this.state.form.languageNative
											?
										this.state.form.languageNative
											:
										this.props.user.info.languageNative 
											? 
										this.props.user.info.languageNative 
											: 
										''
									}
									onChange={this.handleChange('languageNative')}
									firebase={this.props.firebase}
								/>
								<Typography style={{ margin: '10px 0px' }}>Idiomas Estudiandos y Aprendidos</Typography>
								<SelectLanguage 
									id="view/language"
									disabled={disabled}
									className={disabled ? "input-view" : ""}
									value={
										form.language
											?
										form.language
											:
										user 
											? 
										user.info.language 
											: 
										''
									}
									onChange={this.handleChange('language')}
									firebase={this.props.firebase}
								/>
							</Grid>
						</Grid>
						<button type="submit" id="profile/language" style={{display: 'none'}}>Enviar</button>
					</form>
				</div>
			</Paper>
		)
	}
}

export default Language;