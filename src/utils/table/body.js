import React, { Component } from 'react';
import {
	Avatar,
	Button,
	Icon,
	TableBody,
	TableRow,
	TableCell,
	TablePagination
} from '@material-ui/core';

const Users = (props) => {
	const { 
		docs, 
		collection, 
		i, 
		handleDelete, 
		handleEdit, 
		handleView 
	} = props;
	return (
		<TableRow
			hover
			style={{
				cursor: 'pointer',
			}}
		>
			<TableCell 
				numeric
				onClick={event => handleView(event, docs.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{i+1}
			</TableCell>
			<TableCell 
				padding="dense"
				onClick={event => handleView(event, docs.id)}
			>
				{
					collection.photo
						?
					<Avatar src={collection.photo} alt={collection.username}/>
						:
					<Icon style={{ fontSize: 40 }}>account_circle</Icon>
				}
			</TableCell>
			<TableCell
				onClick={event => handleView(event, docs.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.username}
			</TableCell>
			<TableCell 
				onClick={event => handleView(event, docs.id)}
				padding="none" style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>{collection.fullname}</TableCell>
			<TableCell
				onClick={event => handleView(event, docs.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.email}
			</TableCell>
			<TableCell 
				onClick={event => handleView(event, docs.id)}
				padding="none" 
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{
					collection.priv === 1 ? 'Administrador'
						:
					collection.priv === 2 ? 'Supervisor'
						:
					collection.priv === 3 ? 'Usuario'
						:
					null
				}
			</TableCell>
			<TableCell padding="none">
				<Button 
					variant="raised" 
					size="small" 
					color="primary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleEdit(event, 2, docs.id)}
				>
					<Icon>edit</Icon>
				</Button>
				<Button 
					variant="raised" 
					size="small" 
					color="secondary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleDelete(collection.username, docs.id)}
				>
					<Icon>delete_forever</Icon>
				</Button>
			</TableCell>
		</TableRow>
	)
}
const LanguagesCurrentMoney = (props) => {
	const { 
		docs, 
		collection, 
		i, 
		handleDelete, 
		handleEdit, 
		handleView 
	} = props;
	return (
		<TableRow
			hover
			style={{
				cursor: 'pointer',
			}}
		>
			<TableCell 
				numeric
				onClick={event => handleView(event, docs.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{i+1}
			</TableCell>
			{/* <TableCell 
				padding="dense"
				onClick={event => handleView(event, docs.id)}
			>
				{
					collection.photo
						?
					<Avatar src={collection.photo} alt={collection.username}/>
						:
					<Icon style={{ fontSize: 40 }}>account_circle</Icon>
				}
			</TableCell> */}
			<TableCell
				onClick={event => handleView(event, docs.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.name}
			</TableCell>
			<TableCell 
				onClick={event => handleView(event, docs.id)}
				padding="none" style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.acronym}
			</TableCell>
			<TableCell padding="none">
				<Button 
					variant="raised" 
					size="small" 
					color="primary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleEdit(event, 2, docs.id)}
				>
					<Icon>edit</Icon>
				</Button>
				<Button 
					variant="raised" 
					size="small" 
					color="secondary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleDelete(collection.name, docs.id)}
				>
					<Icon>delete_forever</Icon>
				</Button>
			</TableCell>
		</TableRow>
	)
}

class Body extends Component {
	state = {}
	render(){
		const { 
			collection, 
			template, 
			handleEdit, 
			handleDelete, 
			handleView 
		} = this.props;
		return (
			<TableBody>
				{
					collection.length > 0
						?
					collection.map((docs, i) => {
						let collection = docs.data();
						switch(template){
							case 'users':
								return 	<Users 
													key={i} 
													docs={docs} 
													collection={collection} 
													i={i} 
													handleEdit={handleEdit} 
													handleDelete={handleDelete} 
													handleView={handleView}
												/>
							case 'languagesCurrentMoney':
								return 	<LanguagesCurrentMoney
													key={i} 
													docs={docs} 
													collection={collection} 
													i={i} 
													handleEdit={handleEdit} 
													handleDelete={handleDelete} 
													handleView={handleView}
											 	/>
						}
					})
					:
					<TableRow>
						<TableCell>No se ha encontrado </TableCell>
					</TableRow>
				}
			</TableBody>
		)
	}
}

export default Body;